/** Class representing page1, where all the main actions happen */
// Function that creates the table
function generateTable() {
    /**
     * Creating variables and assigning elements to them
     */
    let num_row = document.getElementById("row");
    let num_col = document.getElementById("col");
    let tableSpaceZone = document.getElementById("table-render-space");
    let newTable = document.createElement("table");
    let tableBody = document.createElement("tbody");

    let w = document.getElementById("tableWidth");
    let bgcolor = document.getElementById("bgColor");
    let textColor = document.getElementById("textColor");
    let borderColr = document.getElementById("borderColor");
    let borderWidth = document.getElementById("borderWidth");

    let htmlCodeSpace = document.querySelector("#table-html-space");
    //resetting "table-render-space", to avoid duplicating tables
    tableSpaceZone.innerHTML = "";


    //This forloop generates the table, and applies styles to it.
    for (let r = 0; r < num_row.value; r++) {
        let tr = document.createElement("tr");
        for (let c = 0; c < num_col.value; c++) {
            let td = document.createElement("td");
            let cell = document.createTextNode("Cell" + r + "" + (c + 1));
            td.appendChild(cell);
            tr.appendChild(td);
            //Calls functions 
            changeTableWidth(w, newTable);
            changeBackground(bgcolor, newTable);
            changeTextColor(textColor, td);
            changeBorderColor(borderColr, newTable);
            changeBorderWidth(borderWidth, newTable);

            w.addEventListener("blur", function () {
                w = document.getElementById("tableWidth").value;
                changeTableWidth(w, newTable);
            });

            bgcolor.addEventListener("blur", function () {
                bgcolor = document.querySelector("#bgColor").value;
                changeBackground(bgcolor, newTable);
            });

            borderColr.addEventListener("blur", function () {
                borderColr = document.querySelector("#borderColor").value;
                changeBorderColor(borderColr, td);
            });
            borderWidth.addEventListener("blur", function () {
                borderWidth = document.querySelector("#borderWidth").value;
                changeBorderWidth(borderWidth, td);
            });
            textColor.addEventListener("blur", function () {
                textColor = document.querySelector("#textColor").value;
                changeTextColor(textColor, td)
            });
            tableBody.appendChild(tr);
        }
        newTable.appendChild(tableBody);
        tableSpaceZone.appendChild(newTable);
    }
    /**
     * These two EventListeners will update value of rows and columns when changed. 
     */
    num_row.addEventListener("blur", function () {
        num_row = document.getElementById("row");
        generateTable();

    });

    num_col.addEventListener("blur", function () {
        num_col = document.getElementById("col");
        generateTable();

    });
    /**
     * This calls the generateHtmlCode each time a row or column has been changed.
     */

    //resetting "table-html-space", to avoid duplicating textArea Elements
    if (document.querySelector("textarea")) {
        document.querySelector("textarea").remove();
    }
    generateHtmlCode();


}
//Function that creates text area including the source code of table-render-space
function generateHtmlCode() {
    /**
     * Assigning elements to variables.
     */

    //Creating textarea element
    let renderSpace = document.querySelector("#table-render-space");
    let htmlCodeSpace = document.querySelector("#table-html-space");
    let txtArea = document.createElement("textarea");
    //copying code from createTable onto textArea
    txtArea.textContent = renderSpace.innerHTML;
    htmlCodeSpace.appendChild(txtArea);
}
// Calling GenerateTable Function.
document.addEventListener('DOMContentLoaded', function () {
    document.addEventListener("blur", generateTable());

});


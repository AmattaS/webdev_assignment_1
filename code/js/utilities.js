// we create functions that will grab information from the forms so that 
// we can send to table_generator.js
    /**
     * Style table: change width
     * @param  {value} width - The width value.
     * @param {element} table - The table value.
     */
function changeTableWidth(width,table){
    table.style.width = width+"%";
}
    /**
     * Style table background: change Color
     * @param {value} color- The color value.
     * @param {element} table - The tableBg value.
     */
function changeBackground(color,tableBg){
    tableBg.style.backgroundColor = color;
}
    /**
     * Style table text: change color
     * @param {value} color - The color value.
     * @param {element} td    - The textColr value.
     */
function changeTextColor(color, textColr){
    textColr.style.color = color;
}
    /**
     * Style table border: change color
     * @param  {value} color - The borderColor value.
     * @param {element} td - The tableBdColr value.
     */
function changeBorderColor(borderColor, tableBdColr){
    tableBdColr.style.borderColor = borderColor;
}
    /**
     * Style table border: change width
     * @param {value} width - The borderWidth value.
     * @param {element} td - The tableBorderWdth value.
     */
function changeBorderWidth(borderWidth, tableBorderWdth){
    tableBorderWdth.style.borderWidth = borderWidth+"px";
}